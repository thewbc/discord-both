from discord.ext import commands
import os
import discord
import time 
import asyncio
from keep_alive import keep_alive

messages = joined = 0
bot = commands.Bot(command_prefix='?')

@bot.command()
async def test(ctx, *args):
    await ctx.send('{} arguments: {}'.format(len(args), ', '.join(args)))


def read_token():
    with open("tok.txt", "r") as f:
        lines = f.readlines()
        return lines[0].strip()

token = read_token()

client  = discord.Client()


async def update_stats():
    await client.wait_until_ready()
    global messages, joined

    while not client.is_closed():
        try:
            with open("stats.txt", "a") as f:
                f.write(f"Time: {int(time.time())}, Messages: {messages}, Members Joined: {joined}\n")

            messages = 0
            joined = 0
            
            await asyncio.sleep(5)
        except Exception as e:
            print(e)
            await asyncio.sleep(5)


@client.event
async def on_member_join(member):
        for channel in member.server.channels:
            if str(channel) == "general":
                await channel.send_message(f"""Oh glob parties over, looks like {member.mention} is tring to join""")



#SENDS all messages to konsole
@client.event
async def on_message(message):
    id = client.get_guild(ID HERE)
    channels = ["commands"]
    valid_users = ["ME"]



client.loop.create_task(update_stats())
#token = os.environ.get("DISCORD_BOT_SECRET")
client.run("TOKENHERE")
